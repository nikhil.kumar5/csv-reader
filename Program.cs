﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CsvReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] csvLine = File.ReadAllLines(@"C:\Users\nikhil.kumar\Desktop\CSV\AssetTracking.csv");
            string[] deviceName = new string[csvLine.Length - 2];
            int[] chargeValue = new int[csvLine.Length-2];
            int[] DJC = new int[csvLine.Length-2];

            for (int i = 2; i < csvLine.Length; i++)
            {
                string[] rowData = csvLine[i].Split(',');
                deviceName[i - 2] = rowData[0];
                string s1 = rowData[4];
                if (s1 == "N/A")
                    s1 = "0";
                chargeValue[i - 2] = Int32.Parse(s1);
                string s2 = rowData[11];
                if (s2== "N/A")
                    s2 = "0";
                DJC[i-2] = Int32.Parse(s2);
            }
            int maxcharge = -100;
            int index1 = -1;
            int maxDJC = -100;
            int index2 = -1;
            for (int i = 0; i < csvLine.Length - 2; i++)
            {
                if(maxcharge<chargeValue[i])
                {
                    maxcharge = chargeValue[i];
                    index1 = i;
                }
                if(maxDJC<DJC[i])
                {
                    maxDJC = DJC[i];
                    index2 = i;
                }
            }
            Console.WriteLine("Device " + deviceName[index1] + " have maximum battery charge which is: " + maxcharge);
            Console.WriteLine("Device " + deviceName[index2] + " have maximum deployed jobs count which is: " + maxDJC);
            Console.ReadKey();
        }
    }
}
